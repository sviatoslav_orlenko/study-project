const INITIAL_PRIZE = 0;
const INITIAL_RANGE_MAX = 8;
const INITIAL_RANGE_MIN = 0;
const INITIAL_PRIZE_MULTIPLIER = 1;
const RANGE_ADDITION = 4;
const WIN_PRIZE_MULTIPLIER = 2;


if (confirm('Do you want to play a game?')){
    playGame(INITIAL_PRIZE, INITIAL_RANGE_MAX, INITIAL_PRIZE_MULTIPLIER);
} else {
    alert('You did not become a billionaire, but can.')
}


function playGame(currentPrize, range, prizeMultiplier){
    let roundPrize = playRound(currentPrize, range, prizeMultiplier);
    if(roundPrize === 0){
        alert('Thank you for your participation. Your prize is: '+currentPrize + '$');
        playAgainQuestion(currentPrize);
    } else{
        currentPrize += roundPrize;
        if(confirm('Congratulation, you won! Your prize is: ' +roundPrize + '$. Do you want to continue?')){
            playGame(currentPrize, range + RANGE_ADDITION, prizeMultiplier * WIN_PRIZE_MULTIPLIER);
        } else {
            alert('Thank you for your participation. Your prize is: '+ currentPrize +'$')
            playAgainQuestion(currentPrize);
        }
    }
}

function playAgainQuestion(currentPrize){
    if(confirm('Want to play again?')){
        playGame(currentPrize, INITIAL_RANGE_MAX, INITIAL_PRIZE_MULTIPLIER);
    } else {
        return;
    }
}

function playRound(currentPrize, range, prizeMultiplier){
    
    for(let i=3; i>0; i--){
        let rouletteResult = getRandomBetween(INITIAL_RANGE_MIN, range);
        let possiblePrize = getPossiblePrize(i, prizeMultiplier);
        let userInput = parseInt(prompt(
            'Choose a roulette pocket number from 0 to '+ range + '\n' +
            'Attempts left: '+i + '\n' +
            'Total prize:' +currentPrize + '$\n' +
            'Possible prize on current attempt: '+ possiblePrize +'$'
            ));
        console.log('Roulette result='+rouletteResult);
        if(userInput === rouletteResult){
            return possiblePrize;
        } 
    } 
    return 0;
}
function getRandomBetween(min, max) {
    let random = Math.random() * (max - min) + min;
    return Math.round(random);
  }
  
function getPossiblePrize(attemptsLeft, prizeMultiplier){
    switch(attemptsLeft){
        case 3: return 100 * prizeMultiplier;
        case 2: return 50 * prizeMultiplier;
        case 1: return 25 * prizeMultiplier;
        default: 0;
    }
   }

  

 
