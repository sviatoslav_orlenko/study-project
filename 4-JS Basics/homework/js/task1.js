 function calculate(){
    let initialAmount= parseInt(document.getElementsByName('initialAmount')[0].value);
    let numberOfYears= parseInt(document.getElementsByName('numberOfYears')[0].value);
    let percentageOfYear= parseInt(document.getElementsByName('percentageOfYear')[0].value);
    if (!initialAmount||initialAmount<1000){
        alert('Invalid data - deposit less then 1000.');
        return
    }
    if (!numberOfYears||numberOfYears<1){
        alert('Invalid data - number of years less then one.');
        return
    }
    if (!percentageOfYear||percentageOfYear>100||percentageOfYear<0){
        alert('Invalid data - Percentage should be between 0 and 100 ');
        return
    }
    let totalAmount= initialAmount;
    for(let i=0; i<numberOfYears; i++){
        let oneYearProfit = percentageOfYear * totalAmount / 100;
        totalAmount= totalAmount+oneYearProfit;
    }    
    let totalProfit= totalAmount-initialAmount;
    alert(
        'Initial amount: ' +initialAmount + '\n' +
        'Number of years: ' +numberOfYears + '\n' + 
        'Percentage of year: '+percentageOfYear + '\n\n' +
        'Total profit: '+Math.round(totalProfit*100)/100 + '\n' +
        'Total amount: '+Math.round(totalAmount*100)/100 
    )
}
